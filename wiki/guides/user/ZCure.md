---
title: ZCure
description: Guide how to setup BL:R with ZCure.
---

ZCure is a complete rewrite of the original game servers which provide the same experience as before the PC servers were shutdown in 2019.

It is extremely easy to setup and requires no additional software to be installed.

!!! info "Beta"
    The ZCure server is currently in Beta and only supports a subset of the original game features.
    However you can create accounts, configure your profile and loadouts and join our community servers already.

## Setup

!!! warning "After first login, cancel account migration!"

    After the first login it will ask you to migrate your account. **Click on "cancel" to deny, otherwise the client will crash afterwards!**

!!! warning "Not coming from Steam?"
    If you downloaded the BL:R client elsewhere than steam, you have to create `[BLR]/Binaries/Win32/steam_appid.txt` with `480` as content!

=== ":material-steam: Using Steam"

    1. Add BLR to your Library (do this even if you installed BLR through steam)
        1. open steam client
        1. navigate to `Games` -> `Add a Non-Steam Game to My Library`
        1. click on `Browse`
        1. open `FoxGame-win32-Shipping.exe` in the `[BLR]/Binaries/Win32` directory
        1. click on `Add Selected Programs`
    1. Configure BLR
        1. in your Library, right-click on `FoxGame-win32-Shipping.exe` and select `Properties`
        1. *(optional)* change the name from `FoxGame-win32-Shipping.exe` to `BLRevive`
        1. paste the text below into the `Launch Options` textbox:
            ```
            -zcureurl=blrrevive.ddd-game.de -zcureport=80 -presenceurl=blrrevive.ddd-game.de -presenceport=9004
            ```


=== ":material-link: Using a Shortcut"

    1. open the file explorer and navigate to `[BLR]/Binaries/Win32`
    1. right-click on `FoxGame-win32-Shipping.exe` and chose `Create Shortcut`
    1. right-click the shortcut and click `Properties`
    1. select the `Shortcut` tab and **add** (not override) `-zcureurl=blrrevive.ddd-game.de -zcureport=80 -presenceurl=blrrevive.ddd-game.de -presenceport=9004` to the `Target` textbox


=== ":material-console-line: Using CLI"

    === ":material-microsoft-windows: Windows"

        ``` batch title="open CMD.exe in BLR/Binaries/Win32"
        start .\FoxGame-win32-Shipping.exe^
            -zcureurl=blrrevive.ddd-game.de -zcureport=80^
            -presenceurl=blrrevive.ddd-game.de -presenceport=9004
        ```

    === ":simple-linux: Linux"

        ``` bash title="open shell in BLR/Binaries/Win32"
        wine ./FoxGame-win32-Shipping.exe \
            -zcureurl=blrrevive.ddd-game.de -zcureport=80 \
            -presenceurl=blrrevive.ddd-game.de -presenceport=9004
        ```

## Features


<div class="grid cards" markdown>

- :material-check-all: **advantages**

    ---

    - **easy setup**: only need to start the original game with different parameters.
    - **vanilla**: does not require additional software.
    - **ingame UI**: original experience as before the PC servers shutdown.
    - **device independant**: configuration like settings and loadouts are stored on the server and automatically synced between different devices.

- :material-close: **downsides**

    ---

    - **no offline/lan server**: requires internet connection and does not support offline/lan servers
    - **loadout limit**: currently only 50 loadouts per account are supported (this is a limit from the BL:R client; might change in future)
</div>


---
title: contribution
author: BLRevive
---
# User Contribution

If you want to contribute to the repository here's a list of things you can do.

- Spread the word
- Help and Discuss with other users in our [Discord](https://discord.gg/wwj9unRvtN)
- [Host game servers](./../guides/hosting/game-server)
- [Help with development](./../../dev/getting-started.md)
- and, most importantly, **Play!** 🎮

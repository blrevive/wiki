# Base configuration directory

In your game server's Config directory, which should probably be something like:

* `/srv/blacklightre/config` for Linux Artemis/Docker setups
* `/srv/blacklightre/game/FoxGame/Config/` for Linux manual setups
* `C:\ProgramData\blacklightre\config` for Windows Artemis setups
* `C:\ProgramData\blacklightre\game\FoxGame\Config` for Windows manual setups

... and shall now be henceforth be referred to as `config/`, create a directory named `BLRevive`.

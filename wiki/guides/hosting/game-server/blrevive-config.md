# BLRevive configuration

In `config/BLRevive/`, create a file named `myserver.json`, which will contain BLRevive's general config:

```json
{
    "Console": {
        "CmdBlacklist": [],
        "CmdWhitelist": [],
        "Enable": false
    },
    "Logger": {
        "FilePath": "blrevive-{server}{timestamp}.log",
        "Level": "trace",
        "Target": "file"
    },
    "Modules": {
        "server-utils": {
            "PatchFile": "patches.json",
            "RulesFile": "rules.json"
        }
    },
    "Server": {
        "AuthenticateUsers": false,
        "Enable": true
    }
}
```

This JSON file's name has to match the `?Config=` (or `GameSettings.ConfigName` or `BLREVIVE_GAMESETTINGS_CONFIGNAME`) parameter you pass to the game server. If you rename it to anything else, make sure to change it appropriately.
